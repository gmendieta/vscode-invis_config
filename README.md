# VS Code Invis Config Extension

This extension implements a language server for the configuration files used in Invis software.

# Running the Extension

- Run `npm install` in terminal to install dependencies
- Run the `Run Extension` target in the Debug View. This will:
	- Start a task `npm: watch` to compile the code
	- Run the extension in a new VS Code window

# Package the Extension

- Run `npm install` in terminal to install dependencies
- Run `vsce package` in terminal to create vsix package, which may be shared to install the extension in other machines

# Extension commands

The extension registers three commands. Two of them are required to initialize the language server properly

- **Invis - Set Config Entrypoint** defines the entry file and the parameters of the configuration. Both the entry file and the parameters are saved in the workspace

- **Invis - Show Config Entrypoint** shows an information window with the entry file and parameters saved in the workspace if any

- **Invis - Reload Config** reloads the configuration using the entry file and parameters saved in the workspace. This command forces the server to read all the configuration files from the filesystem, instead of using the internal cache

# Language server features

## Semantic tokens

In addition to the syntaxtic rules defined by the extension, the language server provides a more precise semantic tokens which allows the editor to colorize the different tokens

## Go to definition

Allows the user to efficiently move through the definitions and included files across configuration filess

## Hover information

Placing the mouse over a defined symbol makes the extension show a tooltip when the actual value of the symbol

![Hover information](website/image/hover.png)

## Search references

Allows the user to easily find all the references of a specific symbol inside the configuration

## Diagnostics

The language server analyzes the whole configuration and provides the editor with several warnings and errors that allows the user to find problems while editing the configuration files

![Diagnostics](website/image/warnings.png)

## Active blocks

The configuration language gives the user the ability to use #ifdef #else #endif blocks, allowing a configuration to work for different environments, based on specific definitions provided. Thus, the language server colorize disabled blocks with different opacity making it easy for the user to understand the configuration

![Active blocks](website/image/active_blocks.png)

## Workspace symbols

The language server provides the editor the complete symbol list defined in the configuration. Thus, the user is able to navigate easily to an specific symbol

![Workspace symbols](website/image/workspace_symbols.png)

