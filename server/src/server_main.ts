import { createConnection, ProposedFeatures } from 'vscode-languageserver/node';
import { VsCodeOutputLogger } from './logging';
import { Server } from './server';

// Create a connection for the userver, using Node's ipc as transport
const connection = createConnection(ProposedFeatures.all);

let logger = new VsCodeOutputLogger(connection);

let server = new Server(connection, logger);
server.start();
