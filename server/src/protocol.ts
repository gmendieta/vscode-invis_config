import { RequestType } from 'vscode-languageserver';
import { TextDocumentItem } from 'vscode-languageserver';
import { Range } from 'vscode-languageserver';

//#region to server
export const setEntryPoint = new RequestType<{ uri: string, params: string[] }, void, any>('icfg/setEntryPoint');

export const getEntryPoint = new RequestType<{}, { uri: string, params: string[] }, any>('icfg/getEntryPoint');

export const getDisabledCodeRanges = new RequestType <{ uri: string }, Range[], any>('icfg/getDisabledCodeRanges');

/*
export const getSemanticTokens = new RequestType<{ uri: string }, number[] | undefined, any>('icfg/getSemanticTokens');

export const getSemanticTokensLegend = new RequestType<{}, {types: string[], modifiers: string[]} | undefined, any>('icfg/getSemanticTokensLegend');
*/

//#region from server
export const getTextDocument = new RequestType<{ uri: string }, TextDocumentItem | undefined, any>('icfg/getTextDocument');

export const updateDisabledCodeRanges = new RequestType<{}, void, any>('icfg/updateDisabledCodeRanges');
