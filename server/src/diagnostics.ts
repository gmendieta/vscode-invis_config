import { DiagnosticSeverity } from "vscode-languageserver-types";
import { DiagnosticLevel } from "./config/diagnostics";

export function transformDiagnosticSeverityLevel(level: DiagnosticLevel): DiagnosticSeverity {
    switch (level) {
        case DiagnosticLevel.Error:
            return DiagnosticSeverity.Error;
        case DiagnosticLevel.Warning:
            return DiagnosticSeverity.Warning;
        case DiagnosticLevel.Info:
            return DiagnosticSeverity.Information;
        case DiagnosticLevel.Hint:
            return DiagnosticSeverity.Hint;
        default:
            return DiagnosticSeverity.Information;
    }
}
