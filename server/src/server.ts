import { Diagnostic, InitializeParams, InitializeResult } from 'vscode-languageserver/node';
import { Connection, Location, Range, SymbolInformation, SymbolKind, TextDocumentSyncKind, SemanticTokensParams } from 'vscode-languageserver';
import { ILogger } from "./config/types/logger";
import * as protocol from './protocol';
import { VsTextDocumentProvider } from './text_document_provider';
import { ConfigManager } from './config/config_manager';
import { StrUtils } from './config/str_utils';
import { URI } from 'vscode-uri';
import { DefineHistory } from './config/define_history';
import { DefineData, TokenType, TokenData, DefineOpType, IncludeData } from './config/metadata';
import { transformDiagnosticSeverityLevel } from './diagnostics';
import { TextDocument } from "vscode-languageserver-textdocument";
import { TextDocuments } from 'vscode-languageserver/node';
import { Mutex } from 'async-mutex';
import * as fs from 'fs';
import { SemanticTokensEncoder } from './config/semantic_tokens_encoder';
import { Diagnostic as ConfigDiagnostic} from './config/diagnostics';

const kSemanticTokenTypes: string[] = ['class', 'struct', 'property', 'variable', 'string', 'number'];
const kSemanticTokenModifiers: string[] = [];

export class Server {

    private config: ConfigManager;
    private documents: TextDocuments<TextDocument>
    private textDocumentProvider: VsTextDocumentProvider;
    private semanticTokenEncoder: SemanticTokensEncoder;

    private parseMutex = new Mutex();
    private isConfigDirty = false;
    private configTimer: NodeJS.Timer | undefined;

    constructor(
        private connection: Connection,
        private logger: ILogger
    ) {
        this.documents = new TextDocuments(TextDocument);
        this.textDocumentProvider = new VsTextDocumentProvider(this.connection, this.documents, this.logger);
        this.config = new ConfigManager(this.textDocumentProvider, this.logger);
        this.semanticTokenEncoder = new SemanticTokensEncoder(kSemanticTokenTypes, kSemanticTokenModifiers);
    }

    release() {
        if (this.configTimer != undefined) {
            clearInterval(this.configTimer);
        }
    }

    start(): void {
        // Check if we need 
        this.configTimer = setInterval(async () => {
            if (this.isConfigDirty) {
                if (!this.parseMutex.isLocked()) {
                    const uri = this.config.getRootFileUri();
                    const params = this.config.getParameters();
                    this.parseConfig(uri, params);
                }
            }
        }, 2500);

        this.textDocumentProvider.onDidChangeDocumentContent(() => {
            this.isConfigDirty = true;
        });

        this.connection.onInitialize((params: InitializeParams) => {
            const result: InitializeResult = {
                capabilities: {
                    textDocumentSync: TextDocumentSyncKind.Incremental,
                    /*
                    diagnosticProvider: {
                        documentSelector: null,
                        identifier: 'icfg',
                        interFileDependencies: true,
                        workspaceDiagnostics: false,
                    },
                    */
                    hoverProvider: true,
                    definitionProvider: true,
                    referencesProvider: true,
                    workspaceSymbolProvider: true,
                    semanticTokensProvider: {
                        documentSelector: null,
                        legend: { tokenTypes: kSemanticTokenTypes, tokenModifiers: kSemanticTokenModifiers },
                        full: true,
                        range: false,
                    }
                }
            };
            return result;
        });

        this.connection.onInitialized(() => {
            this.logger.log('onInitialized');
        });

        this.connection.onShutdown(() => {
            this.release();
        });

        this.connection.onExit(() => {
            this.release();
        });

        this.connection.onDidChangeConfiguration(event => {
            this.logger.log('onDidChangeConfiguration');
        });

        this.connection.onRequest(protocol.setEntryPoint, params => {
            this.logger.log('onRequest setEntryPoint');
            this.parseMutex
                .waitForUnlock()
                .then(() => {
                    this.config.clear(true, true);
                    this.parseConfig(params.uri, params.params,);
                });
        });

        this.connection.onRequest(protocol.getEntryPoint, params => {
            this.logger.log('onRequest getEntryPoint');
            return {
                uri: this.config.getRootFileUri(),
                params: this.config.getParameters()
            };
        });

        /*
        this.connection.onRequest(protocol.getSemanticTokensLegend, params => {
            this.logger.log('onRequest getSemanticTokensLegend');
            return {
                types: kSemanticTokenTypes,
                modifiers: kSemanticTokenModifiers
            };
        })

        this.connection.onRequest(protocol.getSemanticTokens, params => {
            this.logger.log('onRequest getSemanticTokens');

            let semanticTokens: number[] = [];
            let uri = URI.file(params.uri);
            let parser = this.config.getParser(uri.fsPath);
            if (parser != undefined) {
                for (const token of parser.getLocalTokens()) {
                    switch (token.type) {
                        case TokenType.ConfigKey:
                            semanticTokens.push(token.line + 1, token.startColumn - 1, token.lexeme.length, 0, 0);
                            break;
                        case TokenType.ConfigValue:
                        case TokenType.IncludeParameter:
                            if (token.value != undefined) {
                                semanticTokens.push(token.line + 1, token.startColumn - 1, token.lexeme.length, 1, 0);
                            }
                            break;
                        case TokenType.SectionName:
                            semanticTokens.push(token.line + 1, token.startColumn - 1, token.lexeme.length, 2, 0);
                            break;
                        default:
                            break;
                    }
                }
            }
            return semanticTokens;
        });
        */

        this.connection.onRequest(protocol.getDisabledCodeRanges, (params) => {
            const ranges: Range[] = [];

            let uri = URI.parse(params.uri);
            let parser = this.config.getParser(uri.fsPath);
            if (parser != undefined) {
                const ifDefBlocks = parser.getIfDefBlocks();
                ifDefBlocks.forEach(ifDefBlock => {
                    ifDefBlock.sections.forEach(IfDefSection => {
                        if (!IfDefSection.active) {
                            ranges.push(Range.create(
                                IfDefSection.startLine,
                                0,
                                IfDefSection.endLine,
                                0
                            ));
                        }
                    });
                });
            }

            return ranges;
        });

        this.connection.onRequest('textDocument/semanticTokens/full', (params: SemanticTokensParams) => {
            this.logger.log('onRequest textDocument/semanticTokens/full');

            let semanticTokens: number[] = [];
            let uri = URI.parse(params.textDocument.uri);
            let parser = this.config.getParser(uri.fsPath);
            if (parser != undefined) {
                semanticTokens = this.semanticTokenEncoder.encodeTokens(parser.getLocalTokens());
            }

            return {
                data: semanticTokens
            };
        });

        this.connection.onHover((params, token) => {
            let uri = URI.parse(params.textDocument.uri);
            let parser = this.config.getParser(uri.fsPath);
            if (parser != undefined) {
                // Expand the current position to get the token we are looking for
                const content = parser.getContent();
                const pos = StrUtils.getPosFromLineColumn(content, params.position.line, params.position.character);
                const keySubStr = StrUtils.getExpandedSubStr(content, pos, " ,\t\r\n", " ,\t\r\n");
                const word = keySubStr.content;

                let value: string | undefined;

                let selectedInclude: IncludeData | undefined;
                for (const include of parser.getIncludes()) {
                    if ((selectedInclude == undefined || selectedInclude.line < include.line) && include.line < params.position.line) {
                        selectedInclude = include;
                    }
                }

                if (selectedInclude != undefined) {
                    let includeParser = this.config.getParser(selectedInclude.filePath);
                    let includeDefines = includeParser?.getProcessedDefines();
                    let includeDefineHistory = includeDefines?.get(word);
                    if (includeDefineHistory != undefined) {
                        let lastDefine = includeDefineHistory.getLastDefine();
                        if (lastDefine != undefined) {
                            if (lastDefine.value != undefined) {
                                value = lastDefine.value.length == 0 ? "defined" : lastDefine.value;
                            }
                            else {
                                value = "undefined";
                            }
                        }
                    }
                }
                // We only want to process inherited defines if there is no included file before the line we are searching
                // as the include processed defines already contains this file inherited defines
                else {
                    let inheritedDefines = parser.getInheritedDefines();
                    let inheritedDefineHistory = inheritedDefines.get(word);
                    if (inheritedDefineHistory != undefined) {
                        // Can use the last define of the las inherited history
                        let lastDefine = inheritedDefineHistory.getLastDefine();
                        if (lastDefine != undefined) {
                            if (lastDefine.value != undefined)
                                value = lastDefine.value.length == 0 ? 'defined' : lastDefine.value;
                            else
                                value = 'undefined';
                        }
                    }
                }

                let localDefines = parser.getLocalDefines();
                let localDefineHistory = localDefines.get(word);
                if (localDefineHistory != undefined) {
                    let localDefine: DefineData | undefined;
                    // We are searching for the closest define of the local defines. If a selected include exists, the local define should be posterior to the include
                    for (const define of localDefineHistory.getHistory()) {
                        if ((selectedInclude == undefined || selectedInclude.line < define.line) && define.line <= params.position.line)
                            if (define.value != undefined)
                                value = define.value.length == 0 ? 'defined' : define.value;
                            else
                                value = 'undefined';
                    }
                }

                if (value != undefined) {
                    return {
                        contents: [value]
                    };
                }
            }
            return undefined;
        });

        this.connection.onDefinition((params, token) => {
            let uri = URI.parse(params.textDocument.uri);
            let parser = this.config.getParser(uri.fsPath);
            if (parser != undefined) {
                // Expand the current position to get the token we are looking for
                const content = parser.getContent();
                const pos = StrUtils.getPosFromLineColumn(content, params.position.line, params.position.character);
                const keySubStr = StrUtils.getExpandedSubStr(content, pos, " ,\t\r\n", " ,\t\r\n");
                const word = keySubStr.content;

                // To check includes we are able to check column numbers directly
                for (const include of parser.getIncludes()) {
                    if (params.position.line == include.line && include.startColumn <= params.position.character && params.position.character <= include.endColumn) {
                        let uri = URI.file(fs.realpathSync(include.targetFilePath));
                        return Location.create(
                            uri.path,
                            Range.create(0, 0, 0, 0)
                        );
                    }
                }

                const defineHistory = parser.getProcessedDefines()?.get(word)
                if (defineHistory != undefined) {
                    let defineData: DefineData | undefined;
                    for (const define of defineHistory.getHistory()) {
                        // Check whether we should use this define or not 
                        if (define.filePath != parser.getFilePath() || define.line <= params.position.line) {
                            defineData = define;
                        }
                    }

                    if (defineData != null) {
                        let uri = URI.file(fs.realpathSync(defineData.filePath));
                        return Location.create(
                            uri.path,
                            Range.create(defineData.line, defineData.startColumn, defineData.line, defineData.startColumn)
                        );
                    }
                    return undefined;
                }
            }
        });

        this.connection.onReferences((params, token) => {

            let references: Location[] = [];
            let uri = URI.parse(params.textDocument.uri);
            let parser = this.config.getParser(uri.fsPath);
            if (parser != undefined) {
                // Expand the current position to get the token we are looking for
                const content = parser.getContent();
                const pos = StrUtils.getPosFromLineColumn(content, params.position.line, params.position.character);
                const keySubStr = StrUtils.getExpandedSubStr(content, pos, " ,\t\r\n", " ,\t\r\n");
                const word = keySubStr.content;

                /*
                // If we only want previous defines
                let processedDefines = parser.getProcessedDefines();
                let defineHistory = processedDefines.get(keySubStr.content);
                if (defineHistory != undefined) {
                    for (const defineData of defineHistory.getHistory()) {
                        references.push(
                            new vscode.Location(
                                vscode.Uri.file(defineData.filePath),
                                new vscode.Position(defineData.line, defineData.startColumn)
                            )
                            );
                        }
                    }
                }
                */

                for (const parser of this.config.getParsers()) {
                    let localDefines = parser.getLocalDefines();
                    let defineHistory = localDefines.get(word);
                    if (defineHistory != undefined) {
                        for (const defineData of defineHistory.getHistory()) {
                            uri = URI.file(fs.realpathSync(defineData.filePath));
                            references.push(
                                Location.create(
                                    uri.path,
                                    Range.create(defineData.line, defineData.startColumn, defineData.line, defineData.startColumn)
                                )
                            );
                        }
                    }

                    for (const token of parser.getLocalTokens()) {
                        if (token.lexeme == word) {
                            uri = URI.file(fs.realpathSync(token.filePath));
                            references.push(
                                Location.create(
                                    uri.path,
                                    Range.create(token.line, token.startColumn, token.line, token.startColumn)
                                )
                            );
                        }
                    }
                }

            }

            return references;
        });

        this.connection.onWorkspaceSymbol((params, token) => {
            let symbols: SymbolInformation[] = [];
            const parsers = this.config.getParsers();
            parsers.forEach(parser => {
                const localDefines = parser.getLocalDefines();
                localDefines.forEach((value: DefineHistory, key: string) => {
                    const defineData = value.getLastDefine();
                    if (defineData != undefined) {
                        symbols.push(SymbolInformation.create(
                            key,
                            SymbolKind.Variable,
                            Range.create(defineData.line, defineData.startColumn, defineData.line, defineData.startColumn),
                            URI.file(defineData.filePath).path,
                            ""
                        ));
                    }
                });
            });
            return symbols;
        });

        // Listen on the connection
        this.connection.listen();
    }

    async parseConfig(uri: string, params: string[]) {
        this.parseMutex.acquire();
        this.config.parseConfig(uri, params)
            .then(() => {
                this.updateDiagnostics();
                this.connection.sendRequest(protocol.updateDisabledCodeRanges, {});
            })
            .finally(async () => {
                this.parseMutex.release();
            });
        this.isConfigDirty = false;
    }

    async updateDiagnostics(): Promise<void> {
        this.config.getParsersByPath().forEach((parsers, key) => {
            let longestDiagnostics: ConfigDiagnostic[] = [];
            // For this path, compare the diagnostics of all parsers and get the longest one
            parsers.forEach(parser => {
                let diagnostics = parser.getDiagnostics();
                if (longestDiagnostics.length < diagnostics.length) {
                    longestDiagnostics = diagnostics;
                }
            });

            let serverDiagnotics: Diagnostic[] = [];
            longestDiagnostics.forEach(diag => {
                serverDiagnotics.push(
                    Diagnostic.create(
                        Range.create(diag.startLine, diag.startColumn - 1, diag.endLine, diag.endColumn),
                        diag.message,
                        transformDiagnosticSeverityLevel(diag.level),
                    ) 
                );
            });

            // Send the computed diagnostics to VSCode
            this.connection.sendDiagnostics({
                uri: URI.file(parsers[0].getFilePath()).path,
                diagnostics: serverDiagnotics
            });
        });

        this.logger.log(`connection.sendDiagnostics`);
    }
}