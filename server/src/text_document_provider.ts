import { ITextDocumentProvider } from "./config/types/text_document_provider";
import { Connection, Emitter } from "vscode-languageserver";
import { TextDocument } from "vscode-languageserver-textdocument";
import { TextDocuments } from 'vscode-languageserver/node';
import { TextDocumentItem } from 'vscode-languageserver-types';
import * as protocol from './protocol';
import { ILogger } from "./config/types/logger";
import { URI } from "vscode-uri";
import * as fs from 'fs';
import { resolve } from "path";
import { resolveModulePath } from "vscode-languageserver/lib/node/files";

export class VsTextDocumentProvider implements ITextDocumentProvider {
    private textDocumentCache: Map<string, TextDocumentItem> = new Map<string, TextDocumentItem>();
    
    private readonly _onDidChangeDocumentContent = new Emitter<void>();
    public readonly onDidChangeDocumentContent = this._onDidChangeDocumentContent.event;

    constructor(
        private connection: Connection,
        private documents: TextDocuments<TextDocument>,
        private logger: ILogger
    ) {
        this.documents.onDidSave(event => {
            this.logger.log(`onDidSave ${event.document.uri}`);
        });
        
        this.documents.onDidClose(event => {
            this.logger.log(`onDidClose ${event.document.uri}`);
        });
        
        this.documents.onDidChangeContent(event => {
            this.logger.log(`onDidChangeContent ${event.document.uri}`);
            const uri = URI.parse(event.document.uri);
            const realPath = fs.realpathSync(uri.fsPath).toLowerCase();
            let textDocument = this.textDocumentCache.get(realPath);
            if (textDocument != undefined) {
                textDocument.version = event.document.version;
                logger.log(`Set ${realPath} content`)
                textDocument.text = event.document.getText();

                this._onDidChangeDocumentContent.fire();
            }
        });

        // Make the text document manager listen on connection for 
        // open, change and close text document events
        this.documents.listen(connection);
    }

    getTextDocumentSync(uri: string): TextDocument | undefined {
        return this.documents.get(uri);
    }

    getTextDocumentItemSync(path: string): TextDocumentItem | undefined {
        const realPath = fs.realpathSync(path).toLowerCase();
        if (!this.textDocumentCache.has(realPath)) {
            const fileContent = fs.readFileSync(realPath, 'utf8');
            this.textDocumentCache.set(realPath, TextDocumentItem.create(realPath, 'icfg', 0, fileContent));
        }
        return this.textDocumentCache.get(path);
    }

    async getTextDocumentItem(path: string): Promise<TextDocumentItem | undefined> {
        let realPath = fs.realpathSync(path).toLowerCase();
        if (!this.textDocumentCache.has(realPath)) {
            let fileContent: string;
            try {
                this.logger.log(`Read ${realPath} from disk`)
                fileContent = fs.readFileSync(realPath, 'utf8');
            }
            catch(error) {
                this.logger.error(`${error}`);
                throw error;
            }
            this.textDocumentCache.set(realPath, TextDocumentItem.create(realPath, 'icfg', 0, fileContent));
        }
        return this.textDocumentCache.get(realPath);
    }

    clear(): void {
        this.textDocumentCache.clear();
    }
}
