import { notEqual } from "assert";

/*
For the "C" locale, white-space characters are any of:
' '     (0x20)	space (SPC)
'\t'	(0x09)	horizontal tab (TAB)
'\n'	(0x0a)	newline (LF)
'\v'	(0x0b)	vertical tab (VT)
'\f'	(0x0c)	feed (FF)
'\r'	(0x0d)	carriage return (CR)
*/
const kSpaceChars: string[] = [' ', '\t', '\n', '\v', '\f', '\r'];

export namespace StrUtils {
    export class SubStr {
        start = 0;
        end = 0;
        content = "";

        constructor(start = 0, end = 0, content = "") {
            this.start = start;
            this.end = end;
            this.content = content;
        }
    }

    export function getNextDelimitedSubStr(source: string, position: number, leftLimit: string, rightLimit: string): SubStr {
        let substr = new SubStr(position, position);

        // Skip left limits
        substr.start = findNextCharNotIn(leftLimit, source, substr.start);
        // Search the end of the substr
        substr.end = findNextCharIn(rightLimit, source, substr.start);

        substr.content = source.substring(substr.start, substr.end);
        return substr;
    }

    export function getExpandedRightSubStr(source: string, position: number, rightLimit: string): SubStr {
        let substr = new SubStr(position, position);

        substr.end = findNextCharIn(rightLimit, source, substr.end);

        // Right side of substr is not inclusive so we need to specify the next character
        substr.content = source.substring(substr.start, substr.end + 1);
        return substr;
    }

    export function getExpandedLeftSubStr(source: string, position: number, leftLimit: string): SubStr {
        let substr = new SubStr(position, position);

        // As the left index of substring is inclusive we need to make checks
        if (0 < substr.start && !leftLimit.includes(source[substr.start])) {
            while (0 < substr.start && !leftLimit.includes(source[substr.start - 1]))
                substr.start--;
        }

        substr.content = source.substring(substr.start, substr.end);
        return substr;
    }

    export function getExpandedSubStr(source: string, position: number, leftLimit: string, rightLimit: string): SubStr {
        let substr = new SubStr(position, position);

        // As the left index of substring is inclusive we need to make checks
        if (0 < substr.start && !leftLimit.includes(source[substr.start])) {
            while (0 < substr.start && !leftLimit.includes(source[substr.start - 1]))
                substr.start--;
        }

        substr.end = findNextCharIn(rightLimit, source, substr.end);

        substr.content = source.substring(substr.start, substr.end);
        return substr;
    }

    export function getNumOcurrences(source: string, search: string, start: number, end: number): number {
        let count = 0;
        let index = source.indexOf(search, start);
        while (-1 < index && index < end) {
            count++;
            start = index + 1;
            index = source.indexOf(search, start);
        }
        return count;
    }

    export function findNextCharIn(search: string, source: string, start: number): number
    {
        let pos = start;
        while (pos < source.length && !search.includes(source[pos]))
            pos++;

        return pos;
    }

    export function findNextCharNotIn(search: string, source: string, start: number): number
    {
        let pos = start;
        while (pos < source.length && search.includes(source[pos]))
            pos++;

        return pos;
    }

    // Return 0-based line and 0-based column 
    export function getLineColumnFromPos(source: string, position: number, endLine: string = '\n') {
        let line = getNumOcurrences(source, endLine, 0, position);
        let column = 0;
        let posLine = source.lastIndexOf(endLine, position);
        if (posLine < source.length)
            column = position - posLine;
        else
            column = position;

        return [line, column] as const;
    }

    export function getPosFromLineColumn(source: string, line: number, column: number, endLine: string = '\n') {
        let numLine = 0;
        let numColumn = 0;
        let pos = 0;
        // Move to proper line
        while (numLine < line && pos < source.length) {
            if (source[pos] == endLine)
                numLine++;
            pos++;
        }
        // Move to proper column without changing line
        while (numColumn < column && pos < source.length) {
            // We are already in the proper line. Do not change again
            if (source[pos] == endLine)
                break;

            numColumn++;
            pos++;
        }
        return pos;
    }

    export function replaceAll(source: string, search: string, target: string) {
        let re = new RegExp(search);
        return source.replace(re, target);
    }
    
    // Remove all characters behind // from any string. It does not check endlines
    export function removeSingleLineCommentFromStr(source: string): string {
        let pos = 0;
        while (pos < source.length && (source[pos] != '/' || source[pos + 1] != '/'))
            pos++;

        return source.substring(0, pos);
    }

    export function removeSingleLineCommentFromSubStr(source: SubStr) {
        let pos = 0;
        while (pos < source.content.length && (source.content[pos] != '/' || source.content[pos + 1] != '/'))
            pos++;
        
        source.content = source.content.substring(0, pos);
        source.end = source.start + source.content.length;
    }

    export function trimLeftSubStr(source: SubStr) {
        let leftPos = 0;
        while (leftPos < source.content.length && kSpaceChars.includes(source.content[leftPos]))
            leftPos++;

        source.content = source.content.substring(leftPos);
        source.start += leftPos;
    }

    export function trimRightSubStr(source: SubStr) {
        let rightPos = source.content.length - 1;
        while (rightPos > 0 && kSpaceChars.includes(source.content[rightPos]))
            rightPos--;

        source.content = source.content.substring(0, rightPos + 1);
        source.end = source.start + source.content.length;
    }

    export function trimSubStr(source: SubStr) {
        trimLeftSubStr(source);
        trimRightSubStr(source);
    }

    export function removeSpacesFromStr(source: string): string {
        let result: string = "";
        for (let i = 0; i < source.length; i++) {
            // Append only non space characters
            if (!kSpaceChars.includes(source[i])) {
                result = result.concat(source[i]);
            }
        }
        return result;
    }
}