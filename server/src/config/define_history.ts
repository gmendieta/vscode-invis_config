import { DefineData } from "./metadata";

export class DefineHistory {
    private key: string = "";
    private history: Array<DefineData> = [];

    constructor(key: string) {
        this.key = key;
    }

    copy(): DefineHistory {
        let clone = new DefineHistory(this.key);
        for (const data of this.history) {
            // IMPORTANT Store a reference
            clone.history.push(data);
        }

        return clone;
    }

    getKey(): string {
        return this.key;
    }

    getLastDefine(): DefineData | undefined {
        if (this.history.length > 0)
            return this.history[this.history.length - 1];

        return undefined;
    }

    getHistory(): Array<DefineData> {
        return this.history;
    }

    add(define: DefineData) {
        // Guess the key is correct
        this.history.push(define);
    }

    concat(defineHistory: DefineHistory) {
        // Guess the key is correct
        for (const define of defineHistory.history) {
            this.add(define);
        }
    }
}