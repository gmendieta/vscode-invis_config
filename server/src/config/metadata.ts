import { BlobOptions } from 'buffer';
import { CommentThreadCollapsibleState } from 'vscode';
import { ConfigFileParser } from './config_file_parser';

export enum TokenType {
    Unknown = 0,
    EOL,
    EOF,
    Equal,
    OpenCurlyBracket,
    CloseCurlyBracket,

    SectionName,
    ConfigKey,
    ConfigValue,
    Include,
    IncludePath,
    IncludeParameter,
}

export class TokenData {
    lexeme = "";
    type: TokenType = TokenType.Unknown;
    filePath = "";
    line = 0;
    startColumn = 0;
    /*
    Some tokens might have an associated value which is itself one or more tokens. For instance, when a #define
    is used as a value. The token member stores the original key, and the value member stores the substitution value
    */
    value?: Array<string>;

    addValue(value: string) {
        if (this.value == undefined)
            this.value = new Array<string>();

        this.value?.push(value);
    }

    copy(): TokenData {
        let clone = new TokenData();

        clone.lexeme = this.lexeme;
        clone.type = this.type;
        clone.filePath = this.filePath;
        clone.line = this.line;
        clone.startColumn = this.startColumn;
        clone.value = this.value;

        return clone;
    }

    str(): string {
        const filePath = this.filePath;
        const line = this.line;
        return `${filePath} (${line})`;
    }
}

// Class which represents a range of an array. It stores a reference of an array and the indices representing
// the range it uses. The begin index is inclusive and the end index is exclusive
export class GenericSpan<T> {
    data: Array<T>;
    begin: number = 0;
    end: number = 0;

    constructor(data: Array<T>, beginIdx: number, endIdx: number) {
        this.data = data;
        this.begin = beginIdx;
        this.end = endIdx;
    }

    empty(): boolean {
        return (this.data == undefined || this.begin == this.end);
    }

    length(): number {
        return this.end - this.begin;
    }
}

export class IncludeData {
    included = false;
    filePath = "";
    line = 0;
    startColumn = 0;
    endColumn = 0;
    targetFilePath = "";

    copy(): IncludeData {
        let clone = new IncludeData();

        clone.included = this.included;
        clone.filePath = this.filePath;
        clone.line = this.line;
        clone.startColumn = this.startColumn;
        clone.endColumn = this.endColumn;
        clone.targetFilePath = this.targetFilePath;

        return clone;
    }
}

export enum DefineOpType {
    Define = 0,
    Undefine
}

export class DefineData {
    filePath = "";
    line = 0;
    startColumn = 0;
    endColumn = 0;
    operation: DefineOpType = DefineOpType.Define;
    key: string = "";
    value: string | undefined = undefined;

    copy(): DefineData {
        let clone = new DefineData();
        clone.filePath = this.filePath;
        clone.line = this.line;
        clone.startColumn = this.startColumn;
        clone.endColumn = this.endColumn;
        clone.operation = this.operation;
        clone.key = this.key;
        clone.value = this.value;

        return clone;
    }
}

export enum IfDefSectionType {
    IfDef = 0,
    IfNotDef,
    ElifDef,
    ElifNotDef,
    IfFalse,
    IfTrue,
    Else
}


export class IfDefSectionData {
    active = false;
    type = IfDefSectionType.IfDef;
    startLine = 0;
    endLine = 0;
}

export class IfDefBlockData {
    sections : Array<IfDefSectionData> = [];
    activeSectionIdx: number | undefined = undefined;
    startLine = 0;
    endLine = 0;

    constructor(section: IfDefSectionData) {
        this.startLine = section.startLine;
        this.addSection(section);
    }

    hasActiveSection(): boolean {
        return this.activeSectionIdx != undefined;
    }

    addSection(section: IfDefSectionData) {
        if (section.active)
            this.activeSectionIdx = this.sections.length;

        this.sections.push(section);
    }

    getLastSection(): IfDefSectionData {
        return this.sections[this.sections.length - 1];
    }

    hasElse(): boolean {
        for (const section of this.sections) {
            if (section.type == IfDefSectionType.Else) {
                return true;
            }
        }
        return false;
    }
}

export class OverrideData
{
    filePath = "";
    line = 0;
    startColumn = 0;
    endColumn = 0;
    targetSection = "";
    targetProp = "";
    targetValue = "";
    hasCondition = false;
    conditionProp: string | undefined = undefined;
    conditionValue: string | undefined = undefined;
}

