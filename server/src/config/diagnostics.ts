export enum DiagnosticLevel {
    Error = 0,
    Warning,
    Info,
    Hint
}

export class Diagnostic {
    constructor(
        public level: DiagnosticLevel,
        public message: string,
        public startLine: number,
        public startColumn: number,
        public endLine: number,
        public endColumn: number
    )
    {}
}