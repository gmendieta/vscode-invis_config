
import { blob } from "stream/consumers";
import { TokenType, TokenData } from "./metadata";

interface TokenIdentity {
    type: number,
    modifiers: number
};

export class SemanticTokensEncoder {

    constructor(
        private tokenTypes?: string[],
        private tokenModifiers?: string[]
    ) {

    }

    encodeTokens(tokens: TokenData[]): number[] {

        const orderedTokens = tokens.sort((t1, t2) => t1.line - t2.line || t1.startColumn - t2.startColumn);

        const encodedTokens: number[] = [];
        let prevLine = 0;
        let prevChar = 0;
        orderedTokens.forEach((token) => {
            if (prevLine !== token.line) {
                prevChar = 0;
            }

            if (this.isTokenAvailableToEncode(token)) {
                const tokenType = this.getTokenType(token);

                encodedTokens.push(
                    token.line - prevLine,
                    (token.startColumn - 1) - prevChar,
                    token.lexeme.length,
                    tokenType.type,
                    tokenType.modifiers
                )

                prevLine = token.line;
                prevChar = token.startColumn - 1;
            }
        });

        return encodedTokens;

    }

    private isTokenAvailableToEncode(token: TokenData): boolean  {
        return (
            token.type == TokenType.ConfigKey ||
            ((token.type == TokenType.ConfigValue || token.type == TokenType.IncludeParameter) && (token.value != undefined)) ||
            token.type == TokenType.SectionName
        );
    }

    private getTokenType(token: TokenData): TokenIdentity {

        let type: number = 0;
        let modifiers: number = 0;

        switch (token.type) {
            case TokenType.ConfigKey:
                type = 2;
                modifiers = 0;
                break;
            case TokenType.ConfigValue:
            case TokenType.IncludeParameter:
                if (token.value != undefined) {
                    type = 3;
                    modifiers = 0;
                }
                break;
            case TokenType.SectionName:
                type = 0;
                modifiers = 0;
                break;
            default:
                break;
        }

        return {
            type: type,
            modifiers: modifiers
        };
    }



}