
export interface ILogger {
    log(message: string): void;
    warning(message: string): void;
    error(message: string): void;
}