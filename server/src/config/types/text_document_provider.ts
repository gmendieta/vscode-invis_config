import { TextDocumentItem } from "vscode-languageserver-types";

export interface ITextDocumentProvider {
    getTextDocumentItemSync(uri: string): TextDocumentItem | undefined;
    getTextDocumentItem(uri: string): Promise<TextDocumentItem | undefined>;
    clear(): void;
}