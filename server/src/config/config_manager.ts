import { ConfigFileParser } from './config_file_parser';
import { ConfigSection } from './config_section';
import * as data from './metadata';
import { StrUtils } from './str_utils';
import { ILogger } from './types/logger';
import { Diagnostic, DiagnosticLevel } from './diagnostics';
import { ITextDocumentProvider } from './types/text_document_provider';
import * as fs from 'fs';

export class ConfigManager {

    private kValidTokenChars = "ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyz0123456789_#.:-";

    private rootFileUri: string = "";
    private rootParser: ConfigFileParser | undefined;

    // Storage for all parsers
    private parsers: Array<ConfigFileParser> = [];
    private parsersByPath: Map<string, Array<ConfigFileParser>> = new Map<string, Array<ConfigFileParser>>();
    private tokens: Array<data.TokenData> = []
    private sections: Map<string, Array<ConfigSection>> = new Map<string, Array<ConfigSection>>();
    private parameters: Array<string> = [];
    private overrides: Array<data.OverrideData> = [];

    constructor(
        private textDocumentProvider: ITextDocumentProvider,
        private logger: ILogger
    ) {

    }

    clear(data = true, provider = false): void {
        if (data === true) {
            this.parsers = [];
            this.parsersByPath.clear();
            this.tokens = []
            this.sections.clear();
            this.parameters = [];
            this.overrides = [];
        }
        if (provider === true) {
            this.textDocumentProvider.clear();
        }
    }

    getRootFileUri(): string {
        return this.rootFileUri;
    }

    getParameters(): string[] {
        return this.parameters;
    }

    async parseConfig(uri: string, params?: Array<string>) {
        this.logger.log(`### Start parsing config ###`)
        // Clear internal data
        this.clear();

        this.rootFileUri = uri;
        this.rootParser = new ConfigFileParser(this.textDocumentProvider, this.logger);

        if (params != undefined)
            this.parameters = params;

        // Add config defines to the root parser
        if (this.parameters != undefined) {
            for (const param of this.parameters) {
                let defineData = new data.DefineData();
                defineData.key = param;
                // Set the rootFilePath as filePath so when using Go To Reference or Go To Definition it goes somewhere
                defineData.filePath = this.rootFileUri;
                this.rootParser.addDefine(defineData);
            }
        }

        // The root parser do not have neither parent parser nor input parameters
        await this.rootParser.parseConfigFile(this.rootFileUri, undefined, undefined, this.overrides);

        // Store all parsers in a sequence
        this.parsers.push(this.rootParser);
        this.parsers = this.parsers.concat(this.rootParser.getChildParsers(true));

        // Organize all the parsers by the path
        for (const parser of this.parsers) {
            const pathLower = parser.getFilePath().toLowerCase();
            let parsersArray = this.parsersByPath.get(pathLower);
            if (parsersArray == undefined) {
                parsersArray = new Array<ConfigFileParser>();
                this.parsersByPath.set(pathLower, parsersArray);
            }
            parsersArray.push(parser);
        }

        this.logger.log(`### Finish parsing config ###`)
        this.logger.log(`${this.parsers.length} file parsers created`);

        // Process tokens from all parsers to generate config sections.
        // All tokens are stored in this.tokens
        this.logger.log(`### Start processing tokens ###`)
        this.processTokens();
        this.logger.log(`### Finish processing tokens ###`)
        this.logger.log(`${this.sections.size} config sections found`);
    }

    getRootParser(): ConfigFileParser | undefined {
        return this.rootParser;
    }

    getParsers(): Array<ConfigFileParser> {
        return this.parsers;
    }

    getParsersByPath(): Map<string, Array<ConfigFileParser>> {
        return this.parsersByPath;
    }

    getParser(filePath: string): ConfigFileParser | undefined {
        // IMPORTANT Some little tricks related with symbolink links and path case. This will work only on Windows
        const realPath = fs.realpathSync(filePath);
        return this.parsers.find(parser => {
            const realPathLower = realPath.toLowerCase();
            const parserPathLower = fs.realpathSync(parser.getFilePath()).toLowerCase(); 
            return realPathLower == parserPathLower;
        });
    }

    private processTokens(): boolean {

        let currentTokenIdx = 0;
        let skipParser = false;

        // Process all tokens parser by parser in order to generate proper diagnostics
        for (const parser of this.parsers) {
            let diagnostics = parser.getDiagnostics();
            currentTokenIdx = this.tokens.length;
            skipParser = false;
            this.tokens = this.tokens.concat(parser.getLocalTokens());
            // Process tokens from the parser of this iteration
            while (currentTokenIdx < this.tokens.length) {
                let token = this.tokens[currentTokenIdx];
                if (token.type == data.TokenType.EOL ||
                    token.type == data.TokenType.Include ||
                    token.type == data.TokenType.IncludePath ||
                    token.type == data.TokenType.IncludeParameter) {
                    currentTokenIdx++;
                    continue;
                }

                // Looking for the name of a section
                if ("{}=".includes(token.lexeme)) {
                    console.error("Waiting for section name. Found %s. %s", token.lexeme, token.str());
                    diagnostics.push(
                        new Diagnostic(
                            DiagnosticLevel.Error,
                            "Waiting for section name",
                            token.line,
                            token.startColumn,
                            token.line,
                            token.startColumn + token.lexeme.length
                        )
                    )
                    // Do not continue analyzing this parser
                    break;
                }

                if (StrUtils.findNextCharNotIn(this.kValidTokenChars, token.lexeme, 0) < token.lexeme.length) {
                    console.error("Found invalid section name %s. %s", token.lexeme, token.str());
                    diagnostics.push(
                        new Diagnostic(
                            DiagnosticLevel.Error,
                            "Invalid section name",
                            token.line,
                            token.startColumn,
                            token.line,
                            token.startColumn + token.lexeme.length
                        )
                    )
                    // Do not continue analyzing this parser
                    break;
                }

                // Create the section
                let section = new ConfigSection(token.lexeme);
                token.type = data.TokenType.SectionName;

                // Store the token idx which will be used when creating the section
                const beginSectionTokenIdx = currentTokenIdx;

                currentTokenIdx++;
                while (currentTokenIdx < this.tokens.length && this.tokens[currentTokenIdx].type == data.TokenType.EOL) {
                    currentTokenIdx++;
                }

                if (this.tokens.length <= currentTokenIdx) {
                    console.error("Found section name without a section. %s. %s", token.lexeme, token.str());
                    // Do not continue analyzing this parser
                    break;
                }

                token = this.tokens[currentTokenIdx];
                if (token.type != data.TokenType.OpenCurlyBracket) {
                    console.error("Waiting for {. Found %s. %s", token.lexeme, token.str());
                    diagnostics.push(
                        new Diagnostic(
                            DiagnosticLevel.Error,
                            "Waiting for section start symbol {",
                            token.line,
                            token.startColumn,
                            token.line,
                            token.startColumn + token.lexeme.length
                        )
                    )
                    // Do not continue analyzing this parser
                    break;
                }

                // Enter the section and process its values
                currentTokenIdx++;
                while (currentTokenIdx < this.tokens.length) {
                    token = this.tokens[currentTokenIdx];
                    if (token.type == data.TokenType.CloseCurlyBracket)
                        break;

                    // Process the values inside a section
                    if (token.type == data.TokenType.EOL) {
                        currentTokenIdx++;
                        continue;
                    }

                    if (token.type == data.TokenType.Equal) {
                        console.error("Found = without a previous name. %s", token.str());
                        diagnostics.push(
                            new Diagnostic(
                                DiagnosticLevel.Error,
                                "Found symbol = without a previos name",
                                token.line,
                                token.startColumn,
                                token.line,
                                token.startColumn + token.lexeme.length
                            )
                        )
                        skipParser = true;
                        break;
                    }

                    if (StrUtils.findNextCharNotIn(this.kValidTokenChars, token.lexeme, 0) < token.lexeme.length) {
                        console.error("Found invalid value name %s. %s", token.lexeme, token.str());
                        diagnostics.push(
                            new Diagnostic(
                                DiagnosticLevel.Error,
                                "Invalid name",
                                token.line,
                                token.startColumn,
                                token.line,
                                token.startColumn + token.lexeme.length
                            )
                        )
                        skipParser = true;
                        break;
                    }

                    // At this point, the token is a valid name
                    let key = token.lexeme;
                    token.type = data.TokenType.ConfigKey;

                    let value = "";
                    currentTokenIdx++;
                    let equalSymbolFound = false;
                    while (currentTokenIdx < this.tokens.length) {
                        token = this.tokens[currentTokenIdx];
                        if (token.type == data.TokenType.EOL || token.type == data.TokenType.CloseCurlyBracket)
                            break;

                        // El signo = es opcional, pero no puede haber 2
                        if (token.type == data.TokenType.Equal) {
                            if (value != "" || equalSymbolFound) {
                                console.error("Found invalid symbol =. %s", token.str());
                                diagnostics.push(
                                    new Diagnostic(
                                        DiagnosticLevel.Error,
                                        "Symbol = cannot appear twice",
                                        token.line,
                                        token.startColumn,
                                        token.line,
                                        token.startColumn + token.lexeme.length
                                    )
                                )
                                skipParser = true;
                                break;
                            }
                            else {
                                equalSymbolFound = true;
                            }
                        }
                        else {
                            if (value != "")
                                value = value + " ";

                            if (token.value != undefined) {
                                for (const v of token.value) {
                                    value = value + v;
                                }
                            }
                            else {
                                value = value + token.lexeme;
                            }
                            token.type = data.TokenType.ConfigValue;
                        }
                        currentTokenIdx++;
                    }

                    if (skipParser) {
                        break;
                    }

                    // Store the key value pair in the section
                    section.addValue(key, value);
                    currentTokenIdx++;
                }

                if (skipParser) {
                    break;
                }

                // Store the token idx which will be used when creating the section
                const endSectionTokenIdx = currentTokenIdx;

                // Store the span of tokens which forms this config section
                section.setTokenSpan(new data.GenericSpan<data.TokenData>(
                    this.tokens,
                    beginSectionTokenIdx,
                    endSectionTokenIdx
                ));

                currentTokenIdx++;

                // Store the section
                let sections = this.sections.get(section.getName());
                if (sections == undefined) {
                    sections = new Array<ConfigSection>();
                    this.sections.set(section.getName(), sections);
                }
                sections.push(section);

                this.addSectionOverrideDiagnostics(section, diagnostics, this.overrides);

            } // while (currentTokenIdx < this.tokens.length) {
        } // or (const parser of this.parsers) {

        return true;
    }

    private addSectionOverrideDiagnostics(section: ConfigSection, diagnostics: Array<Diagnostic>, overrides: Array<data.OverrideData>) {
        const tokens = section.getTokenSpan()
        if (tokens == undefined) {
            return;
        }

        let targetKeyToken: data.TokenData | undefined;
        let targetValueToken: data.TokenData | undefined;
        for (const override of overrides) {
            targetKeyToken = undefined;
            if (override.targetSection != section.getName()) {
                continue;
            }

            if (section.getNumValuesCalled(override.targetProp) == 0) {
                continue;
            }

            // Check override conditionsif any
            if (override.hasCondition) {
                if (override.conditionProp == undefined || override.conditionValue == undefined) {
                    continue;
                }
                const value = section.getRawConfigValue(override.conditionProp, 0);
                if (value != override.conditionValue) {
                    continue;
                }
                else {
                    section.setOrCreateValue(override.targetProp, override.targetValue);
                }
            }
            // Apply the override if there is no condition
            else {
                section.setOrCreateValue(override.targetProp, override.targetValue);
            }
            
            // Search for the specific token and add the diagnostic
            let idx = tokens.begin;
            for (idx = tokens.begin; idx < tokens.end; idx++) {
                const token = tokens.data[idx];
                if (token.type == data.TokenType.ConfigKey && token.lexeme == override.targetProp) {
                    targetKeyToken = token;
                    break;
                }
            }
            idx += 1; // Skip the ConfigKey token
            for (; idx < tokens.end; idx++) {
                const token = tokens.data[idx];
                if (token.type != data.TokenType.Equal &&
                    token.type != data.TokenType.ConfigValue) {
                        break;
                }
                
                if (token.type == data.TokenType.ConfigValue) {
                    targetValueToken = token;
                    break;
                }
            }

            // Add the diagnostics
            if (targetKeyToken != undefined && targetValueToken != undefined) {
                diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Info,
                        "This section parameter might modified by an #override",
                        targetKeyToken.line,
                        targetKeyToken.startColumn,
                        targetKeyToken.line,
                        targetKeyToken.startColumn + targetKeyToken.lexeme.length
                    )
                )
            }
        } // for (const override of overrides)
    }
}