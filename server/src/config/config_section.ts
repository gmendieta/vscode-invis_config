import path = require('path');
import * as vscode from 'vscode';
import { ConfigFileParser } from './config_file_parser';
import * as data from './metadata';
import { StrUtils } from './str_utils';

export class ConfigSection {

    private name: string = "";
    private values: Map<string, Array<string>> = new Map<string, Array<string>>;

    // Tokens the section has been built from
    private tokens: data.GenericSpan<data.TokenData> | undefined;

    constructor(name: string) {
        this.name = name;
    }

    getName(): string {
        return this.name;
    }

    setTokenSpan(tokens: data.GenericSpan<data.TokenData>) {
        this.tokens = tokens;
    }

    getTokenSpan(): data.GenericSpan<data.TokenData> | undefined {
        return this.tokens;
    }

    /// Add a value for a specific keyword. It already exists, add another value
    addValue(key: string, value: string) {
        let values = this.values.get(key);
        if (values == undefined) {
            values = new Array<string>();
            this.values.set(key, values);
        }
        values.push(value);
    }

    /// Modify the value of a specific keyword. If it does not exist, do nothing
    setValue(key: string, value: string) {
        let values = this.values.get(key);
        if (values != undefined) {
            values[0] = value;
        }
    }

    /// Modify the value of a specific keyword. If it doesn't exist, creates it
    setOrCreateValue(key: string, value: string) {
        if (this.values.has(key))
            this.setValue(key, value);
        else
            this.addValue(key, value);
    }

    getNumValues(): number {
        return this.values.size;
    }

    getNumValuesCalled(key: string): number {
        let values = this.values.get(key);
        if (values != undefined)
            return values.length;
            
        return 0;
    }

    getRawConfigValue(key: string, pos: number = 0): string|undefined {
        let values = this.values.get(key);
        if (values != undefined && pos < values.length)
            return values[pos];

        return undefined;
    }
}
