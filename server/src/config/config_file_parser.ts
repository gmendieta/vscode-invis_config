import * as path from 'path';
import { StrUtils } from './str_utils';
import { TokenType, TokenData, DefineOpType, DefineData, IncludeData, IfDefBlockData, IfDefSectionData, IfDefSectionType, OverrideData } from './metadata';
import { DefineHistory } from './define_history';
import { ILogger } from './types/logger';
import { Diagnostic, DiagnosticLevel } from './diagnostics';
import { ITextDocumentProvider } from './types/text_document_provider';
import { TextDocumentItem } from 'vscode-languageserver-types';

export class ConfigFileParser {
    private fileUri: string = "";
    private textDocument: TextDocumentItem | undefined;
    private fileContent: string = "";
    private parentParser?: ConfigFileParser = undefined;
    private parameters?: Map<string, string> = undefined;
    private childParsers: Array<ConfigFileParser> = [];
    private includes: Array<IncludeData> = [];
    private inheritedDefines: Map<string, DefineHistory> = new Map<string, DefineHistory>();
    private localDefines: Map<string, DefineHistory> = new Map<string, DefineHistory>();
    //private includedDefines: Map<string, DefineHistory> = new Map<string, DefineHistory>();
    private processedDefines: Map<string, DefineHistory> = new Map<string, DefineHistory>();
    private localTokens: Array<TokenData> = [];
    private ifDefArray: Array<IfDefBlockData> = [];
    private ifDefStack: Array<IfDefBlockData> = []; // Stack to parse IfDef blocks
    private diagnostics: Array<Diagnostic> = [];
    private maxUsedParameter: number | undefined = undefined; // Store the maximum parameter number used

    private fs = require('fs');
    constructor(
        private textDocumentProvider: ITextDocumentProvider,
        private logger: ILogger
    ) {

    }

    getFilePath(): string {
        return this.fileUri;
    }

    getContent(): string {
        return this.fileContent;
    }

    getIncludes(): Array<IncludeData> {
        return this.includes;
    }

    getLocalDefines(): Map<string, DefineHistory> {
        return this.localDefines;
    }

    getInheritedDefines(): Map<string, DefineHistory> {
        return this.inheritedDefines;
    }

    /*
    getIncludedDefines(): Map<string, DefineHistory> {
        return this.includedDefines;
    }
    */

    getProcessedDefines(): Map<string, DefineHistory> {
        return this.processedDefines;
    }

    getLocalTokens(): Array<TokenData> {
        return this.localTokens;
    }

    getIfDefBlocks(): Array<IfDefBlockData> {
        return this.ifDefArray;
    }

    getDiagnostics(): Array<Diagnostic> {
        return this.diagnostics;
    }

    getNumUsedParameters(): number {
        return this.maxUsedParameter != undefined ? this.maxUsedParameter + 1 : 0;
    }

    addDefine(define: DefineData) {
        // Add config defines to the root parser
        let localDefineHist = this.localDefines.get(define.key);
        if (localDefineHist == undefined) {
            localDefineHist = new DefineHistory(define.key);
            this.localDefines.set(define.key, localDefineHist);
        }

        let processedDefineHist = this.processedDefines.get(define.key);
        if (processedDefineHist == undefined) {
            processedDefineHist = new DefineHistory(define.key);
            this.processedDefines.set(define.key, processedDefineHist);
        }

        localDefineHist.add(define);
        processedDefineHist.add(define);
    }

    async parseConfigFile(uri: string, parentParser: ConfigFileParser | undefined, parameters: Array<string> | undefined, overrides: Array<OverrideData>) {
        // this.logger.log(`Parsing file ${uri}`);

        this.fileUri = uri;
        this.parentParser = parentParser;
        await this.textDocumentProvider.getTextDocumentItem(this.fileUri)
            .then((value) => {
                this.textDocument = value;
                /*
                if (this.textDocument == undefined) {
                    this.logger.error(`Error getting file ${this.fileUri}`);
                    return;
                }
                */
            })
            .catch((err) => {
                this.logger.error(`${err}`);
            });

        if (this.textDocument == undefined) {
            this.logger.error(`Error getting file ${this.fileUri}`);
            return;
        }
        
        this.fileContent = this.textDocument.text;
        // If there are parameters, fill the parameter map with the expected names
        if (parameters != undefined) {
            this.parameters = new Map<string, string>();
            for (let index = 0; index < parameters.length; index++) {
                const param = "%PARAMETER:" + index + "%";
                this.parameters.set(param, parameters[index]);
            }
        }

        if (this.parentParser != undefined) {
            for (const define of this.parentParser.getProcessedDefines()) {
                // IMPORTANT inheritedDefines is a copy of the DefineHistory
                // See DefineHistory.copy for more information
                this.inheritedDefines.set(define[0], define[1].copy());
                this.processedDefines.set(define[0], define[1].copy());
            }
        }

        // Parse file content
        let currentPos: number;
        let lastPos: number;

        currentPos = 0;
        while (currentPos < this.fileContent.length) {
            // Directives
            if (this.fileContent[currentPos] == '#') {
                await this.processDirective(this.fileContent, currentPos, overrides)
                    .then((value) => {
                        currentPos = value;
                    });
            }
            // Comments
            else if (this.fileContent[currentPos] == '/' || this.fileContent[currentPos] == '*') {
                currentPos = this.processComment(this.fileContent, currentPos);
            }
            // New line and others
            else if (this.fileContent[currentPos] == '\n' ||
                this.fileContent[currentPos] == '=' ||
                this.fileContent[currentPos] == '{' ||
                this.fileContent[currentPos] == '}') {
                if (this.isInsideActiveBlock()) {
                    let tokenStr = this.fileContent.substring(currentPos, currentPos + 1);
                    const [numLine, numColumn] = StrUtils.getLineColumnFromPos(this.fileContent, currentPos);

                    let token = new TokenData();
                    token.lexeme = tokenStr;
                    token.filePath = this.fileUri;
                    token.line = numLine;
                    token.startColumn = numColumn;
                    switch (this.fileContent[currentPos]) {
                        case '\n':
                            token.type = TokenType.EOL;
                            break;
                        case '=':
                            token.type = TokenType.Equal;
                            break;
                        case '{':
                            token.type = TokenType.OpenCurlyBracket;
                            break;
                        case '}':
                            token.type = TokenType.CloseCurlyBracket;
                            break;
                        default:
                            token.type = TokenType.Unknown;
                            break;
                    }
                    this.localTokens.push(token);
                }
                currentPos += 1;
            }
            else {
                // Find the end of the token from current position
                let tokenEndPos = this.findEndToken(this.fileContent, currentPos);
                if (this.fileContent.length < tokenEndPos)
                    tokenEndPos = this.fileContent.length;

                let tokenStr = this.fileContent.substring(currentPos, tokenEndPos);
                // Store the original length before possible substitutions
                let tokenLength = tokenStr.length;
                // TODO Check printable

                if (this.isInsideActiveBlock()) {
                    // Process the token, substituting parameters and defines by values.
                    // This function adds tokens to this.localTokens
                    this.processToken(tokenStr, currentPos);
                }

                // Increment current position
                currentPos += tokenLength > 0 ? tokenLength : 1;
            }

            // Skip blank spaces
            currentPos = StrUtils.findNextCharNotIn(" \t", this.fileContent, currentPos);
        }
    }

    private async processDirective(content: string, currentPos: number, overrides: Array<OverrideData>): Promise<number> {
        const commandSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, " \t\r\n", " \t\r\n");
        const [cmdLine, cmdColumn] = StrUtils.getLineColumnFromPos(content, commandSubStr.start);
        const cmdStr = commandSubStr.content;
        currentPos = commandSubStr.end;
        if (cmdStr == "#include") {
            // Get the rest of the line after #include
            const lineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, "\r\n", "\r\n");
            currentPos = lineSubStr.end;
            const [line, column] = StrUtils.getLineColumnFromPos(content, lineSubStr.start);

            // Remove the single line comment if exists
            StrUtils.removeSingleLineCommentFromSubStr(lineSubStr);

            // Separate the filename from the possible parameters
            const fileSubStr = StrUtils.getNextDelimitedSubStr(lineSubStr.content, 0, " ,\t", " ,\t");
            const paramPos = fileSubStr.end;
            const pathSubStr = StrUtils.getNextDelimitedSubStr(fileSubStr.content, 0, "\"", "\"");
            let fileUri = pathSubStr.content;
            if (!path.isAbsolute(fileUri))
                fileUri = path.join(path.dirname(this.fileUri), fileUri);

            // Generate tokens
            let includeToken = new TokenData();
            includeToken.filePath = this.fileUri;
            includeToken.lexeme = cmdStr;
            includeToken.line = line;
            includeToken.startColumn = commandSubStr.start;
            includeToken.type = TokenType.Include;
            this.localTokens.push(includeToken);

            let pathToken = new TokenData();
            pathToken.filePath = this.fileUri;
            pathToken.lexeme = fileSubStr.content;
            pathToken.line = line;
            pathToken.startColumn = column + fileSubStr.start;
            pathToken.type = TokenType.IncludePath;
            this.localTokens.push(pathToken);

            // Split the possible parameters
            let paramSubStr = StrUtils.getNextDelimitedSubStr(lineSubStr.content, paramPos, " ,\t\r\n", ",\r\n");

            let realParameters = new Array<string>();
            while (paramSubStr.content.length != 0) {
                // An include parameter can use parameters of this file
                let param = this.processParameters(paramSubStr.content, currentPos);
                // Now we are substituting the potential defines. Each substitution is able to generate several paramteres
                // because a define A can be subsituted by the tuple 1, 2. So two parameters has been generated from one
                let hasSubstitutedValues = false;
                let paramValues: string[] = [];
                while (param.length != 0) {
                    // Cannot stop in the empty space due to vector values
                    let valueSubStr = StrUtils.getNextDelimitedSubStr(param, 0, " ,\t\r\n", ",\r\n");
                    // We only trim for the right to not disturb line and column in Tokens
                    const valueStr = valueSubStr.content.trim();
                    let defineHist = this.processedDefines.get(valueStr);
                    if (defineHist != undefined) {
                        const defineData = defineHist.getLastDefine();
                        if (defineData != null && defineData.value != undefined) {
                            // Substitution of the new parameters with the old ones
                            param = defineData.value + param.substring(valueSubStr.end);
                            hasSubstitutedValues = true;
                        }
                        else { // Empty define??
                            // Remove the last parameter from the string
                            param = param.substring(valueSubStr.end);
                        }
                    }
                    else { // Real parameter!!!
                        paramValues.push(valueStr);
                        // Remove the last parameter from the string
                        param = param.substring(valueSubStr.end);
                    }
                }

                // Generate parameter tokens
                let paramToken = new TokenData();
                paramToken.filePath = this.fileUri;
                paramToken.lexeme = paramSubStr.content;
                paramToken.line = line;
                paramToken.startColumn = column + paramSubStr.start;
                paramToken.type = TokenType.IncludeParameter;
                // As we are using paramValues to store even the not substituted values, we check hasValues
                // Store the token values and generate real parameters for included file
                paramValues.forEach((value, index) => {
                    if (hasSubstitutedValues) {
                        paramToken.addValue(value);
                    }
                    realParameters.push(value);
                });

                this.localTokens.push(paramToken);

                paramSubStr = StrUtils.getNextDelimitedSubStr(lineSubStr.content, paramSubStr.end, " ,\t\r\n", ",\r\n");
            }

            let included = false;
            if (this.isInsideActiveBlock()) {
                let parser = new ConfigFileParser(this.textDocumentProvider, this.logger);
                await parser.parseConfigFile(fileUri, this, realParameters, overrides);

                // Check the number of parameters to add a diagnostic if necessary
                const numUsedParams = parser.getNumUsedParameters();
                const numProvidedParams = realParameters.length;
                if (numProvidedParams < numUsedParams) {
                    this.diagnostics.push(
                        new Diagnostic(
                            DiagnosticLevel.Error,
                            `Included file needs ${numUsedParams} parameters. ${numProvidedParams} provided.`,
                            line,
                            column,
                            line,
                            column + lineSubStr.content.length
                        )
                    )
                }
                else if (numUsedParams < numProvidedParams) {
                    this.diagnostics.push(
                        new Diagnostic(
                            DiagnosticLevel.Warning,
                            `Included file uses ${numUsedParams} parameters. ${numProvidedParams} provided.`,
                            line,
                            column,
                            line,
                            column + lineSubStr.content.length
                        )
                    )
                }

                /*
                 * The processed defines of the child parser includes all the defines processed by this config parser
                 * in the moment of inclusion, so processed defines can be substituted
                 */
                this.processedDefines.clear();
                // Mix included defines with already included defines
                for (const define of parser.getProcessedDefines()) {
                    // IMPORTANT includedDefines and processedDefines are copies of the DefineHistory
                    // See DefineHistory.copy for more information
                    /*
                    let includedHistory = this.includedDefines.get(define[0]);
                    if (includedHistory != undefined)
                        includedHistory.concat(define[1]);
                    else
                        this.includedDefines.set(define[0], define[1].copy());
                    */

                    this.processedDefines.set(define[0], define[1].copy());
                }

                this.childParsers.push(parser);
                included = true;
            }

            let includeData = new IncludeData();
            includeData.included = included;
            includeData.filePath = this.fileUri;
            includeData.line = line;
            includeData.startColumn = column;
            includeData.endColumn = column + fileSubStr.content.length;
            includeData.targetFilePath = fileUri;

            this.includes.push(includeData);
        }
        else if (cmdStr == "#define") {
            if (this.isInsideActiveBlock()) {

                // Get the rest of the line after #define
                const lineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, "\r\n", "\r\n");
                currentPos = lineSubStr.end;
                const [line, column] = StrUtils.getLineColumnFromPos(content, lineSubStr.start);

                // Remove the single line comment if exists
                StrUtils.removeSingleLineCommentFromSubStr(lineSubStr);

                let keySubStr = StrUtils.getNextDelimitedSubStr(lineSubStr.content, 0, " \t\r\n", " \t\r\n");
                // The value is defined until the end of the line
                let valueSubStr = StrUtils.getNextDelimitedSubStr(lineSubStr.content, keySubStr.end, " \t\r\n", "\r\n");

                let defineData = new DefineData();
                defineData.filePath = this.fileUri;
                defineData.line = line;
                defineData.startColumn = column;
                defineData.endColumn = column + valueSubStr.end;
                defineData.operation = DefineOpType.Define;
                defineData.key = keySubStr.content;
                defineData.value = valueSubStr.content;

                // Include new define both in local and processed defines
                let localHistory = this.localDefines.get(keySubStr.content);
                if (localHistory == undefined) {
                    localHistory = new DefineHistory(defineData.key);
                    this.localDefines.set(keySubStr.content, localHistory);
                }
                let processedHistory = this.processedDefines.get(keySubStr.content);
                if (processedHistory == undefined) {
                    processedHistory = new DefineHistory(defineData.key);
                    this.processedDefines.set(keySubStr.content, processedHistory);
                }

                localHistory.add(defineData);
                processedHistory.add(defineData);
            } // isInsideActiveBlock()
        }
        else if (cmdStr == "#undef") {
            if (this.isInsideActiveBlock()) {

                // Get the rest of the line after #define
                const lineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, "\r\n", "\r\n");
                currentPos = lineSubStr.end;
                const [line, column] = StrUtils.getLineColumnFromPos(content, lineSubStr.start);

                // Remove the single line comment if exists
                StrUtils.removeSingleLineCommentFromSubStr(lineSubStr);

                let keySubStr = StrUtils.getNextDelimitedSubStr(lineSubStr.content, 0, " \t\r\n", " \t\r\n");

                let defineData = new DefineData();
                defineData.filePath = this.fileUri;
                defineData.line = line;
                defineData.startColumn = column;
                defineData.endColumn = column + keySubStr.end;
                defineData.operation = DefineOpType.Undefine;
                defineData.key = keySubStr.content;
                defineData.value = undefined;

                // Include new define both in local and processed defines
                let localHistory = this.localDefines.get(keySubStr.content);
                if (localHistory == undefined) {
                    localHistory = new DefineHistory(defineData.key);
                    this.localDefines.set(keySubStr.content, localHistory);
                }
                let processedHistory = this.processedDefines.get(keySubStr.content);
                if (processedHistory == undefined) {
                    processedHistory = new DefineHistory(defineData.key);
                    this.processedDefines.set(keySubStr.content, processedHistory);
                }

                localHistory.add(defineData);
                processedHistory.add(defineData);
            } // isInsideActiveBlock()
        }
        else if (cmdStr == "#ifdef" || cmdStr == "#ifndef") {
            let defineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, " \t\r\n", " \t\r\n");
            currentPos = defineSubStr.end;
            let isDefined = false;
            let defineHistory = this.processedDefines.get(defineSubStr.content);
            if (defineHistory) {
                let lastDefine = defineHistory.getLastDefine();
                if (lastDefine != undefined && lastDefine.operation != DefineOpType.Undefine) {
                    isDefined = true;
                }
            }

            // Store the #ifdef info
            let ifDefData = new IfDefSectionData();
            ifDefData.startLine = cmdLine;
            ifDefData.type = cmdStr == "#ifdef" ? IfDefSectionType.IfDef : IfDefSectionType.IfNotDef;
            ifDefData.active = (ifDefData.type == IfDefSectionType.IfDef && isDefined) || (ifDefData.type == IfDefSectionType.IfNotDef && !isDefined);

            let ifDefBlock = new IfDefBlockData(ifDefData);
            this.ifDefStack.push(ifDefBlock);
        }
        else if (cmdStr == "#iftrue" || cmdStr == "#iffalse") {
            let defineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, " \t\r\n", " \t\r\n");
            const [defineLine, defineColumn] = StrUtils.getLineColumnFromPos(this.fileContent, defineSubStr.start);
            currentPos = defineSubStr.end;
            const isDefined = this.processedDefines.has(defineSubStr.content);
            if (!isDefined) {
                this.logger.error(`Found undefined value in ${this.fileUri} (${cmdLine})`);
                this.diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Error,
                        `Symbol ${defineSubStr.content} is not defined`,
                        defineLine,
                        defineColumn,
                        defineLine,
                        defineColumn + defineSubStr.content.length
                    )
                )
            }
            else {
                const defineHistory = this.processedDefines.get(defineSubStr.content);
                let defineData = defineHistory?.getLastDefine();
                if (defineData != undefined) {
                    let boolValue: boolean | undefined;
                    const value = defineData.value?.toUpperCase() ?? "";
                    if (value == "TRUE" || value == "YES") {
                        boolValue = true;
                    }
                    else if (value == "FALSE" || value == "NO") {
                        boolValue = false;
                    }
                    else {
                        this.logger.error(`Found invalid boolean value ${this.fileUri} (${cmdLine})`);
                        this.diagnostics.push(
                            new Diagnostic(
                                DiagnosticLevel.Error,
                                `Symbol ${defineSubStr.content} is not a valid boolean value`,
                                defineLine,
                                defineColumn,
                                defineLine,
                                defineColumn + defineSubStr.content.length
                            )
                        )
                    }

                    if (boolValue != undefined) {
                        // Store the #ifdef info
                        let ifDefData = new IfDefSectionData();
                        ifDefData.startLine = cmdLine;
                        ifDefData.type = cmdStr == "#iftrue" ? IfDefSectionType.IfTrue : IfDefSectionType.IfFalse;
                        ifDefData.active = (ifDefData.type == IfDefSectionType.IfTrue && boolValue) || (ifDefData.type == IfDefSectionType.IfFalse && !boolValue);

                        let ifDefBlock = new IfDefBlockData(ifDefData);
                        this.ifDefStack.push(ifDefBlock);
                    }
                }
            }
        }
        else if (cmdStr == "#else") {
            let ifDefBlock: IfDefBlockData | undefined = undefined;
            if (this.ifDefStack.length > 0)
                ifDefBlock = this.ifDefStack[this.ifDefStack.length - 1];

            if (ifDefBlock == undefined) {
                this.logger.error(`Found #else without #if ${this.fileUri} (${cmdLine})`);
                this.diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Error,
                        "Missing #if for this #else block",
                        cmdLine,
                        cmdColumn,
                        cmdLine,
                        cmdColumn + commandSubStr.content.length
                    )
                )
            }
            else if (ifDefBlock.hasElse()) {
                this.logger.error(`Found two #else for the same #if ${this.fileUri} (${cmdLine})`);
                this.diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Error,
                        "Found several #else for the same #if block",
                        cmdLine,
                        cmdColumn,
                        cmdLine,
                        cmdColumn + commandSubStr.content.length
                    )
                )
            }
            else {
                let prevIfDefData = ifDefBlock.getLastSection();
                if (prevIfDefData != undefined)
                    prevIfDefData.endLine = cmdLine;

                // We are inside an else block
                let ifDefData = new IfDefSectionData();
                ifDefData.type = IfDefSectionType.Else;
                ifDefData.startLine = cmdLine;
                ifDefData.active = !ifDefBlock.hasActiveSection();

                ifDefBlock.addSection(ifDefData);
            }
        }
        else if (cmdStr == "#elifdef" || cmdStr == "#elifndef") {
            let ifDefBlock: IfDefBlockData | undefined = undefined;
            if (this.ifDefStack.length > 0)
                ifDefBlock = this.ifDefStack[this.ifDefStack.length - 1];

            if (ifDefBlock == undefined) {
                this.logger.error(`Found #elif without #if ${this.fileUri} (${cmdLine})`);
                new Diagnostic(
                    DiagnosticLevel.Error,
                    "Missing #if for this #elif statement",
                    cmdLine,
                    cmdColumn,
                    cmdLine,
                    cmdColumn + commandSubStr.content.length
                )
            }
            else if (ifDefBlock.hasElse()) {
                this.logger.error(`Found #elif after #else ${this.fileUri} (${cmdLine})`);
                new Diagnostic(
                    DiagnosticLevel.Error,
                    "An #elif block cannot be placed after #else block",
                    cmdLine,
                    cmdColumn,
                    cmdLine,
                    cmdColumn + commandSubStr.content.length
                )
            }
            else {
                let prevIfDefData = ifDefBlock.getLastSection();
                if (prevIfDefData != undefined)
                    prevIfDefData.endLine = cmdLine;

                let defineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, " \t\r\n", "\t\r\n");
                currentPos = defineSubStr.end;

                // Remove the single line comment if exists
                StrUtils.removeSingleLineCommentFromSubStr(defineSubStr);

                const isDefined = this.processedDefines.has(defineSubStr.content);

                let ifDefData = new IfDefSectionData();
                ifDefData.startLine = cmdLine;
                ifDefData.type = cmdStr == "#elifdef" ? IfDefSectionType.ElifDef : IfDefSectionType.ElifNotDef;
                const active = (ifDefData.type == IfDefSectionType.ElifDef && isDefined) || (ifDefData.type == IfDefSectionType.ElifNotDef && !isDefined);
                ifDefData.active = active && !ifDefBlock.hasActiveSection();

                ifDefBlock.addSection(ifDefData);
            }
        }
        else if (cmdStr == "#endif") {
            let ifDefBlock = this.ifDefStack.pop();
            if (ifDefBlock == undefined) {
                this.logger.error(`Found #endif without #if ${this.fileUri} (${cmdLine})`);
                new Diagnostic(
                    DiagnosticLevel.Error,
                    "Missing #if for this #endif statement",
                    cmdLine,
                    cmdColumn,
                    cmdLine,
                    cmdColumn + commandSubStr.content.length
                )
            }
            else {
                ifDefBlock.getLastSection().endLine = cmdLine;
                ifDefBlock.endLine = cmdLine;
                this.ifDefArray.push(ifDefBlock);
            }
        }
        else if (cmdStr == "#error") {
            // Get the rest of the line after #include
            const lineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, "\r\n", "\r\n");
            currentPos = lineSubStr.end;
            if (this.isInsideActiveBlock()) {

            }
        }
        else if (cmdStr == "#trace") {
            // Get the rest of the line after #include
            const lineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, "\r\n", "\r\n");
            currentPos = lineSubStr.end;
            if (this.isInsideActiveBlock()) {

            }
        }
        else if (cmdStr == "#override") {
            currentPos = this.processOverride(this.fileContent, currentPos, overrides);
        }
        else {

        }

        return currentPos;
    }

    private processComment(content: string, currentPos: number): number {
        let originalPos = currentPos;
        let commentBegin = content.substring(currentPos, currentPos + 2);
        currentPos += 2;
        if (commentBegin == "//") {
            const commentSubStr = StrUtils.getExpandedRightSubStr(this.fileContent, originalPos, "\r\n");
            currentPos = commentSubStr.end;
        }
        else if (commentBegin == "*/") {
            // Error
            currentPos += 2;
        }
        else if (commentBegin == "/*") {
            const commentEndPos = this.fileContent.indexOf("*/", originalPos);
            currentPos = commentEndPos != -1 ? commentEndPos : currentPos + 1;
        }
        // It might be a single /
        else {
            // Error
            currentPos += 1;
        }

        return currentPos;
    }

    getChildParsers(recursive: boolean): Array<ConfigFileParser> {
        if (recursive) {
            let parsers: Array<ConfigFileParser> = [];
            for (const parser of this.childParsers) {
                parsers.push(parser);
                parsers = parsers.concat(parser.getChildParsers(recursive));
            }

            return parsers;
        }
        else {
            return this.childParsers;
        }
    }

    private findEndToken(content: string, currentPos: number): number {
        if (content.length <= currentPos)
            return content.length - 1;

        while (true) {
            currentPos = StrUtils.findNextCharIn(" \t\r\n/={}", content, currentPos);

            // Nothing has been found
            if (content.length <= currentPos)
                return content.length - 1;
            // If there is no a bar is the end of the token
            else if (content[currentPos] != "/")
                return currentPos;
            // Is a bar
            else if (currentPos + 1 < content.length) {
                if ((content[currentPos + 1] == '/') || (content[currentPos + 1] == '*'))
                    return currentPos;
                else
                    currentPos++;
            }
        }
    }

    private processParameters(source: string, currentPos: number): string {
        if (source.length == 0)
            return "";

        let tokenStr = source;
        const [numLine, numColumn] = StrUtils.getLineColumnFromPos(this.fileContent, currentPos);

        let paramPos = tokenStr.indexOf("%PARAMETER:");
        while (paramPos != -1) {
            paramPos += 11;
            const endPos = StrUtils.findNextCharNotIn("0123456789", tokenStr, paramPos);
            const paramNum: number = +tokenStr.substring(paramPos, endPos);
            if (this.maxUsedParameter == undefined || this.maxUsedParameter < paramNum)
                this.maxUsedParameter = paramNum;

            paramPos = tokenStr.indexOf("%PARAMETER:", paramPos);
        }

        // Replace all parameters
        if (this.parameters != undefined) {
            for (const param of this.parameters) {
                tokenStr = StrUtils.replaceAll(tokenStr, param[0], param[1]);
            }
        }

        if (tokenStr.search("%PARAMETER:") != -1) {
            this.logger.error(`Found not processed parameter ${source}. ${this.fileUri} (${numLine})`);
            new Diagnostic(
                DiagnosticLevel.Error,
                "This parameter has not been processed. Check the number of parameters",
                numLine,
                numColumn,
                numLine,
                numColumn + 10
            )
        }
        return tokenStr;
    }

    /*
    * Substitutes the token by a parameter or a define
    */
    private processToken(source: string, currentPos: number) {
        let tokenStr = source;
        tokenStr = this.processParameters(source, currentPos);
        const [numLine, numColumn] = StrUtils.getLineColumnFromPos(this.fileContent, currentPos);

        /* Previous substitutions might have convert the token into several tokens
         */
        let tokenPos = 0;
        let tokenSubStr = StrUtils.getNextDelimitedSubStr(tokenStr, tokenPos, " ,\t", " ,\t");
        if (tokenSubStr.content.length == 0)
            return;

        tokenPos = tokenSubStr.end;
        let currentTokenStr = tokenSubStr.content;
        let hasValue = false;
        let defineHist = this.processedDefines.get(currentTokenStr);
        if (defineHist != undefined) {
            const defineData = defineHist.getLastDefine();
            if (defineData != null && defineData.value != undefined) {
                currentTokenStr = defineData.value;
                hasValue = true;
            }
        }

        let token = new TokenData();
        token.filePath = this.fileUri;
        token.lexeme = tokenSubStr.content;
        token.line = numLine;
        token.startColumn = numColumn;
        if (hasValue) {
            let lexemePos = 0;
            let lexemeSubStr = StrUtils.getNextDelimitedSubStr(token.lexeme, lexemePos, " \t", " \t");
            lexemePos = lexemeSubStr.end;

            while (lexemeSubStr.content.length > 0) {
                token.addValue(lexemeSubStr.content);

                lexemeSubStr = StrUtils.getNextDelimitedSubStr(token.lexeme, lexemePos, " \t", " \t");
                lexemePos = lexemeSubStr.end;
            }
        }
        this.localTokens.push(token);
    }

    private isInsideActiveBlock(): boolean {
        let active = true;
        for (const ifdef of this.ifDefStack) {
            active = active && ifdef.getLastSection().active;
        }
        return active;
    }

    private processOverride(content: string, currentPos: number, overrides: Array<OverrideData>): number {
        // Get the rest of the line after #include
        const lineSubStr = StrUtils.getNextDelimitedSubStr(content, currentPos, "\r\n", "\r\n");
        currentPos = lineSubStr.end;
        if (this.isInsideActiveBlock()) {
            const [line, column] = StrUtils.getLineColumnFromPos(content, lineSubStr.start);
            const lineStr = StrUtils.removeSingleLineCommentFromStr(lineSubStr.content);
            /*
            An override is able to have this two forms
            #override SECTION.PARAM = VALUE
            #override SECTION[QUERY_PARAM = QUERY_VALUE].PARAM = VALUE
            */
        
            let valuePos = lineStr.lastIndexOf('=');
            if (valuePos == -1) {
                this.diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Error,
                        `#override syntax is incorrect.`,
                        line,
                        column,
                        line,
                        column + lineSubStr.content.length
                    )
                )
                return currentPos;
            }

            let query = lineStr.substring(0, valuePos);
            // Remove all the empty spaces from the query
            query = StrUtils.removeSpacesFromStr(query);

            // Remove left empty spaces (Vectors contain spaces, i.e. "0.3 1.0 0.8")
            let value = lineStr.substring(valuePos + 1);
            value = value.trimStart();

            const conditionStartPos = query.indexOf('[');
            const conditionEndPos = query.indexOf(']');

            let hasCondition = false;
            let conditionProp = "";
            let conditionValue = "";
            if (conditionStartPos != -1 && conditionEndPos != -1) {
                const conditionSepPos = query.indexOf('=', conditionStartPos);
                conditionProp = query.substring(conditionStartPos + 1, conditionSepPos);
                conditionValue = query.substring(conditionSepPos + 1, conditionEndPos);

                // Remove the condition to continue parsing
                query = query.substring(0, conditionStartPos) + query.substring(conditionEndPos + 1);
                hasCondition = true;
            }
            else if(conditionStartPos != -1 || conditionEndPos != -1) {
                this.diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Error,
                        `#override syntax is incorrect.`,
                        line,
                        column,
                        line,
                        column + lineSubStr.content.length
                    )
                )
                return currentPos;
            }

            const propPos = query.indexOf('.');
            if (propPos == -1) {
                this.diagnostics.push(
                    new Diagnostic(
                        DiagnosticLevel.Error,
                        `#override syntax is incorrect.`,
                        line,
                        column,
                        line,
                        column + lineSubStr.content.length
                    )
                )
                return currentPos;
            }

            const section = query.substring(0, propPos);
            const prop = query.substring(propPos + 1);

            let override = new OverrideData();
            override.filePath = this.fileUri;
            override.line = line;
            override.startColumn = column;
            override.endColumn = column + lineSubStr.content.length;
            override.targetSection = section;
            override.targetProp = prop;
            override.targetValue = value;
            override.hasCondition = hasCondition;
            if (hasCondition) {
                override.conditionProp = conditionProp;
                override.conditionValue = conditionValue;
            }

            overrides.push(override);
        }

        return currentPos;
    }
}