import { Connection } from 'vscode-languageserver';
import { ILogger } from './config/types/logger';

export class VsCodeOutputLogger implements ILogger {

    constructor(
        private connection: Connection
    ) {

    }

    log(message: string): void {
        this.connection.console.log(`[INFO] ${this.now()}] ${message}`);
    }

    warning(message: string): void {
        this.connection.console.warn(`[WARNING] ${this.now()}] ${message}`);
    }

    error(message: string): void {
        this.connection.console.error(`[ERROR] ${this.now()}] ${message}`);
    }

    private now(): string {
        const now = new Date();
        return String(now.getUTCHours()).padStart(2, '0')
            + ':' + String(now.getMinutes()).padStart(2, '0')
            + ':' + String(now.getUTCSeconds()).padStart(2, '0') + '.' + String(now.getMilliseconds()).padStart(3, '0');
    }
}