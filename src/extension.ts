import * as vscode from 'vscode';
import * as path from 'path';
import { VsCodeOutputLogger } from './logging';

import {
    LanguageClient,
    LanguageClientOptions,
    ServerOptions,
    TextDocumentItem,
    TransportKind
} from 'vscode-languageclient/node';

import * as protocol from './client/protocol';
import fs = require('fs');

let logger: VsCodeOutputLogger;
let client: LanguageClient;

export async function activate(context: vscode.ExtensionContext) {
    logger = new VsCodeOutputLogger();

    registerCommands(context);

    context.subscriptions.push(
        vscode.window.onDidChangeVisibleTextEditors(
            updateDisabledCodeDecorations
        )
    )

    // The server is implemented in node
    const serverModule = context.asAbsolutePath(
        path.join('server', 'out', 'server_main.js')
    );

    // Debug options for the server
    // --inspect=6009: runs the server in Node's Inspector mode so VS Code can attach to it
    const debugOptions = {
        execArgv: ['--nolazy', '--inspect=6009']
    };

    const serverOptions: ServerOptions = {
        run: { module: serverModule, transport: TransportKind.ipc },
        debug: {
            module: serverModule,
            transport: TransportKind.ipc,
            options: debugOptions
        }
    };

    // Options to control the language client
    const fileGlob = '**/*.{icfg, cfg}';
    const clientOptions: LanguageClientOptions = {
        documentSelector: [{ scheme: 'file', language: 'icfg' }],
        synchronize: {
            // Notify the server about file changes
            fileEvents: vscode.workspace.createFileSystemWatcher(fileGlob)
        }
    };

    client = new LanguageClient(
        'InvisConfigServer',
        'Invis Config Server',
        serverOptions,
        clientOptions
    );

    client.onRequest(protocol.getTextDocument, params => {
        logger.log(`onRequest getTextDocument ${params.uri}`);
        const fileContent = fs.readFileSync(params.uri, 'utf8');
        return TextDocumentItem.create(params.uri, 'icfg', 0, fileContent);
    });

    client.onRequest(protocol.updateDisabledCodeRanges, params => {
        logger.log(`onRequest updateDisabledCodeRanges`);
        updateDisabledCodeDecorations(vscode.window.visibleTextEditors);
    });

    // Start the client. This will also launch the server
    await client.start();

    // IMPORTANT Semantic token provider is now registered as a server capability
    /*
    // Register semantic tokens provider using client-server request
    client.sendRequest(protocol.getSemanticTokensLegend, {})
        .then(legend => {
            if (legend) {
                const provider: vscode.DocumentSemanticTokensProvider = {
                    provideDocumentSemanticTokens(document) {
                        return client.sendRequest(protocol.getSemanticTokens, { uri: document.uri.path })
                            .then(data => {
                                return data && new vscode.SemanticTokens(new Uint32Array(data));
                            });
                    }
                }
                context.subscriptions.push(
                    vscode.languages.registerDocumentSemanticTokensProvider('icfg', provider, new vscode.SemanticTokensLegend(legend.types, legend.modifiers))
                );
            }
        });
    */
}

export function deactivate(): Thenable<void> | undefined {
    if (client != undefined)
        return client.stop();

    return undefined;
}

function registerCommands(context: vscode.ExtensionContext) {
    const setCmd = vscode.commands.registerCommand('invis_config.set_config_entrypoint', async () => {
        const fileUri = await vscode.window.showOpenDialog({ canSelectFolders: false, canSelectFiles: true, canSelectMany: false, openLabel: 'Select file' });
        if (fileUri) {
            // Always store full path
            // let path = vscode.workspace.asRelativePath(fileUri[0]);
            let path = fileUri[0].fsPath;
            context.workspaceState.update('invis_config.entrypoint', path);
            let prevParams = context.workspaceState.get<string>('invis_config.params');
            if (prevParams == undefined) {
                prevParams = "";
            }
            const params = await vscode.window.showInputBox({
                placeHolder: "Parameters",
                prompt: "Write parameters for config",
                value: prevParams
            });

            context.workspaceState.update('invis_config.params', params);

            client.sendRequest(protocol.setEntryPoint, {
                uri: path,
                params: params?.split(" ")
            });
            logger?.log(`Config file ${path} read`);
        }
    });
    context.subscriptions.push(setCmd);

    const getCmd = vscode.commands.registerCommand('invis_config.get_config_entrypoint', () => {
        let path = context.workspaceState.get<string>('invis_config.entrypoint');
        if (path != undefined) {
            const params = context.workspaceState.get<string>('invis_config.params');
            if (params != undefined) {
                path = path + " " + params;
            }
            vscode.window.showInformationMessage(path);
        }
        else
            vscode.window.showWarningMessage("Invis configuration entry point not defined");
    });
    context.subscriptions.push(getCmd);

    const reloadCmd = vscode.commands.registerCommand('invis_config.reload_config', () => {
        let path = context.workspaceState.get<string>('invis_config.entrypoint');
        if (path != undefined) {
            let params = context.workspaceState.get<string>('invis_config.params');

            client.sendRequest(protocol.setEntryPoint, {
                uri: path,
                params: params?.split(" ")
            });
            logger?.log(`Config file ${path} reload`);
        }
        else {
            vscode.window.showWarningMessage("Invis configuration entry point not defined");
        }
    });
    context.subscriptions.push(reloadCmd);
}

// Disabled code decoration
// IMPORTANT Its important to use the same decoration so vscode will replace the last provided ranges
// with newer provided ones
const disabledCodeDecoration = vscode.window.createTextEditorDecorationType({
    opacity: "0.5",
    backgroundColor: undefined,
    color: undefined,
    rangeBehavior: vscode.DecorationRangeBehavior.OpenClosed
});

function updateDisabledCodeDecorations(editors: readonly vscode.TextEditor[]): void {
    editors.forEach(editor => {
        if ((editor.document.uri.scheme === "file") && (editor.document.languageId === "icfg")) {
            client.sendRequest(protocol.getDisabledCodeRanges, {uri: editor.document.uri.path})
                .then((value) => {
                    let ranges: vscode.Range[] = [];
                    value.forEach(range => {
                        ranges.push(
                            new vscode.Range(
                                range.start.line,
                                range.start.character,
                                range.end.line,
                                range.end.character
                                )
                            );
                        });
                    editor.setDecorations(disabledCodeDecoration, ranges);
                });
            }
        });
    }