import * as vscode from 'vscode';

interface ILogger {
    log(message: string): void;
    warning(message: string): void;
    error(message: string): void;
}

export class VsCodeOutputLogger implements ILogger {
    private outputChannel?: vscode.OutputChannel;

    constructor() {
        this.outputChannel = vscode.window.createOutputChannel('Invis Config', 'icfg');
    }

    log(message: string): void {
        this.outputChannel?.appendLine(`[INFO] ${this.now()}] ${message}`);
    }
    
    warning(message: string): void {
        this.outputChannel?.appendLine(`[WARNING] ${this.now()}] ${message}`);
        this.outputChannel?.append
    }
    
    error(message: string): void {
        this.outputChannel?.appendLine(`[ERROR] ${this.now()}] ${message}`);
    }

    private now(): string {
        const now = new Date();
		return String(now.getUTCHours()).padStart(2, '0')
			+ ':' + String(now.getMinutes()).padStart(2, '0')
			+ ':' + String(now.getUTCSeconds()).padStart(2, '0') + '.' + String(now.getMilliseconds()).padStart(3, '0');
	}
}